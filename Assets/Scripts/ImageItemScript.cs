﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ImageItemScript : MonoBehaviour {

    public Sprite sprite;
    public Image ItemImage;
    public Image MainImageObj;
    public VideoClip videoClip;
    public VideoPlayer MainVideoPlayer;
    public VideoPlayer prevVideoPlayer;
    public GameObject MainVideoPlayerPanel;
    public GameObject Border;

    Button button;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetContent(Sprite _image, Image _mainImageObj, GameObject _videoPlayerPanel)
    {
        ItemImage = GetComponent<Image>();
        button = GetComponent<Button>();
        button.onClick.AddListener(() => OnClickBtn());
        sprite = _image;
        MainImageObj = _mainImageObj;
        ItemImage.sprite = sprite;
        MainVideoPlayerPanel = _videoPlayerPanel;
        /*if (type == 0)
        {
            
        }
        else if (type == 1)
        {

        }*/
    }

    public void SetContent(Image _mainImageObj, VideoPlayer _videoPlayer, GameObject _videoPlayerPanel, VideoClip _videoClip)
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => OnClickVideoBtn());
        videoClip = _videoClip;
        MainImageObj = _mainImageObj;
        MainVideoPlayer = _videoPlayer;
        MainVideoPlayerPanel = _videoPlayerPanel;

        prevVideoPlayer.prepareCompleted += GetVideoPreview;
        prevVideoPlayer.targetTexture.Release();
        prevVideoPlayer.clip = videoClip;
        prevVideoPlayer.time = 3;
        prevVideoPlayer.Prepare();
    }

    public void ShowBorder()
    {
        Border.SetActive(true);
    }

    public void HideBorder()
    {
        Border.SetActive(false);
    }

    void OnClickBtn()
    {
        UIManagerScript.instanse.HideImageBorders();
        MainImageObj.gameObject.SetActive(true);
        MainVideoPlayerPanel.SetActive(false);
        MainImageObj.sprite = sprite;
        ShowBorder();
    }

    void OnClickVideoBtn()
    {
        UIManagerScript.instanse.HideImageBorders();
        MainImageObj.gameObject.SetActive(false);
        MainVideoPlayerPanel.SetActive(true);
        ShowBorder();
    }

    void GetVideoPreview(VideoPlayer _videoPlayer)
    {
        _videoPlayer.Play();
        Loom.QueueOnMainThread(() => {
            _videoPlayer.Pause();
        }, 0.1f);
    }
}
