﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectItem : MonoBehaviour {

    public int Type = 0;
    public PinScript pinScript;

    Button btn;
	// Use this for initialization
	void Start () {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() => OnClickBtn());
        gameObject.transform.GetChild(1).GetComponent<Text>().text = pinScript.Title.ToUpper();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnClickBtn()
    {
        pinScript.OnBtnClick();
    }
}
