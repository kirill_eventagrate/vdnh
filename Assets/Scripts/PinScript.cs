﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PinScript : MonoBehaviour {

    public int type = 0;
    public Button pinBtn;
    public GameObject quad;
    public string Title;
    public Text Desc;
    //public string Desc;
    public Sprite[] Images;
    public VideoClip videoClip;

    Camera m_Camera;

    // Use this for initialization
    void Start () {
        m_Camera = Camera.main;
        pinBtn.onClick.AddListener(() => OnBtnClick());
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    private void LateUpdate()
    {
        quad.transform.LookAt(quad.transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
    }

    public void OnBtnClick()
    {
        if (type == 0)
        {
            MapScript.instanse.HidePins(1f);
            Vector3 newPos = new Vector3(transform.position.x, transform.position.y + 1.2f, transform.position.z - 1.2f);
            CameraScript.instanse.Moving(newPos, 1.8f);
            if (UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().isPanelShow)
            {
                UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().ShowHidePanel();
                UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().wasShow = true;
            }
            
            Loom.QueueOnMainThread(() => {
                UIManagerScript.instanse.ShowPopup(type);
            }, 2.5f);
        }
        else if (type == 1)
        {
            if (UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().isPanelShow)
            {
                UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().ShowHidePanel();
                UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().wasShow = true;
            }
            UIManagerScript.instanse.ShowPopup(type);
        }
        UIManagerScript.instanse.SetContent(Title, Desc.text, Images, videoClip);
    }

    public void ShowPin(float time)
    {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "time", time, "easetype", iTween.EaseType.easeOutQuint));
    }

    public void HidePin(float time)
    {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", time, "easetype", iTween.EaseType.easeOutQuint));
    }
}
