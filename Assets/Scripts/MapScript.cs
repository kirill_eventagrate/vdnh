﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapScript : MonoBehaviour {

    public static MapScript instanse = null;

    public PinScript[] pins;

    private void Awake()
    {
        if (instanse == null)
            instanse = this;
    }

    // Use this for initialization
    void Start () {
        HidePins(0.1f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowPins(float time)
    {
        foreach (PinScript pin in pins)
        {
            pin.ShowPin(time);
        }
    }
    
    public void HidePins(float time)
    {
        foreach (PinScript pin in pins)
        {
            pin.HidePin(time);
        }
    }
}
