﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftPanel : MonoBehaviour {

    [HideInInspector]
    public bool wasShow = false;
    [HideInInspector]
    public bool isPanelShow = false;

    public Button LeftPanelBtn;
	// Use this for initialization
	void Start () {
        LeftPanelBtn.onClick.AddListener(() => ShowHidePanel());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowHidePanel()
    {
        if (!isPanelShow)
        {
            StartCoroutine(ShowLeftPanel());
            isPanelShow = true;
        }
        else
        {
            StartCoroutine(HideLeftPanel());
            isPanelShow = false;
        }
    }
    

    IEnumerator ShowLeftPanel()
    {
        for (float i = 0; i <= 1.1f; i += 0.1f)
        {
            Vector2 newPos = new Vector2(-GetComponent<RectTransform>().sizeDelta.x, 0);
            GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(newPos, Vector2.zero, i);
            yield return null;
        }
    }

    IEnumerator HideLeftPanel()
    {
        for (float i = 0; i <= 1.1f; i += 0.1f)
        {
            Vector2 newPos = new Vector2(-GetComponent<RectTransform>().sizeDelta.x, 0);
            GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(Vector2.zero, newPos, i);
            yield return null;
        }
    }
}
